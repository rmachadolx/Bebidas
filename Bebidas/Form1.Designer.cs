﻿namespace Bebidas
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ButtonCerveja = new System.Windows.Forms.Button();
            this.ButtonAgua = new System.Windows.Forms.Button();
            this.ButtonIcetea = new System.Windows.Forms.Button();
            this.ButtonCocacola = new System.Windows.Forms.Button();
            this.LabelEscolhaBebida = new System.Windows.Forms.Label();
            this.LabelBebida = new System.Windows.Forms.Label();
            this.ComboBoxQuantidade = new System.Windows.Forms.ComboBox();
            this.LabelEscolhaCapacidade = new System.Windows.Forms.Label();
            this.LabelCapacidade = new System.Windows.Forms.Label();
            this.ButtonCriarCopo = new System.Windows.Forms.Button();
            this.LabelMensagem = new System.Windows.Forms.Label();
            this.TrackBarEncheVaza = new System.Windows.Forms.TrackBar();
            this.LabelContem = new System.Windows.Forms.Label();
            this.ButtonVazar = new System.Windows.Forms.Button();
            this.ButtonEncher = new System.Windows.Forms.Button();
            this.ButtonVazarTodo = new System.Windows.Forms.Button();
            this.ButtonEncherTodo = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.LabelValorPercentagen = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarEncheVaza)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonCerveja
            // 
            this.ButtonCerveja.Image = global::Bebidas.Properties.Resources.sagres;
            this.ButtonCerveja.Location = new System.Drawing.Point(338, 62);
            this.ButtonCerveja.Name = "ButtonCerveja";
            this.ButtonCerveja.Size = new System.Drawing.Size(59, 62);
            this.ButtonCerveja.TabIndex = 3;
            this.ButtonCerveja.Text = "c";
            this.ButtonCerveja.UseVisualStyleBackColor = true;
            this.ButtonCerveja.Click += new System.EventHandler(this.ButtonCerveja_Click);
            // 
            // ButtonAgua
            // 
            this.ButtonAgua.Image = global::Bebidas.Properties.Resources.luso;
            this.ButtonAgua.Location = new System.Drawing.Point(235, 62);
            this.ButtonAgua.Name = "ButtonAgua";
            this.ButtonAgua.Size = new System.Drawing.Size(59, 62);
            this.ButtonAgua.TabIndex = 2;
            this.ButtonAgua.UseVisualStyleBackColor = true;
            this.ButtonAgua.Click += new System.EventHandler(this.ButtonAgua_Click);
            // 
            // ButtonIcetea
            // 
            this.ButtonIcetea.Image = global::Bebidas.Properties.Resources.icetea;
            this.ButtonIcetea.Location = new System.Drawing.Point(135, 62);
            this.ButtonIcetea.Name = "ButtonIcetea";
            this.ButtonIcetea.Size = new System.Drawing.Size(59, 62);
            this.ButtonIcetea.TabIndex = 1;
            this.ButtonIcetea.UseVisualStyleBackColor = true;
            this.ButtonIcetea.Click += new System.EventHandler(this.ButtonIcetea_Click);
            // 
            // ButtonCocacola
            // 
            this.ButtonCocacola.Image = ((System.Drawing.Image)(resources.GetObject("ButtonCocacola.Image")));
            this.ButtonCocacola.Location = new System.Drawing.Point(44, 62);
            this.ButtonCocacola.Name = "ButtonCocacola";
            this.ButtonCocacola.Size = new System.Drawing.Size(59, 62);
            this.ButtonCocacola.TabIndex = 0;
            this.ButtonCocacola.UseVisualStyleBackColor = true;
            this.ButtonCocacola.Click += new System.EventHandler(this.ButtonCocacola_Click);
            // 
            // LabelEscolhaBebida
            // 
            this.LabelEscolhaBebida.AutoSize = true;
            this.LabelEscolhaBebida.Location = new System.Drawing.Point(155, 28);
            this.LabelEscolhaBebida.Name = "LabelEscolhaBebida";
            this.LabelEscolhaBebida.Size = new System.Drawing.Size(127, 13);
            this.LabelEscolhaBebida.TabIndex = 4;
            this.LabelEscolhaBebida.Text = "Seleccione a sua bebida:";
            // 
            // LabelBebida
            // 
            this.LabelBebida.AutoSize = true;
            this.LabelBebida.Location = new System.Drawing.Point(41, 136);
            this.LabelBebida.Name = "LabelBebida";
            this.LabelBebida.Size = new System.Drawing.Size(0, 13);
            this.LabelBebida.TabIndex = 5;
            // 
            // ComboBoxQuantidade
            // 
            this.ComboBoxQuantidade.FormattingEnabled = true;
            this.ComboBoxQuantidade.Items.AddRange(new object[] {
            "Escolha a capcidade",
            "20 cl",
            "33 cl",
            "50 cl",
            "75 cl"});
            this.ComboBoxQuantidade.Location = new System.Drawing.Point(157, 187);
            this.ComboBoxQuantidade.Name = "ComboBoxQuantidade";
            this.ComboBoxQuantidade.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxQuantidade.TabIndex = 6;
            this.ComboBoxQuantidade.SelectedIndexChanged += new System.EventHandler(this.ComboBoxQuantidade_SelectedIndexChanged);
            // 
            // LabelEscolhaCapacidade
            // 
            this.LabelEscolhaCapacidade.AutoSize = true;
            this.LabelEscolhaCapacidade.Location = new System.Drawing.Point(154, 158);
            this.LabelEscolhaCapacidade.Name = "LabelEscolhaCapacidade";
            this.LabelEscolhaCapacidade.Size = new System.Drawing.Size(122, 13);
            this.LabelEscolhaCapacidade.TabIndex = 7;
            this.LabelEscolhaCapacidade.Text = "Seleccione a capcidade";
            // 
            // LabelCapacidade
            // 
            this.LabelCapacidade.AutoSize = true;
            this.LabelCapacidade.Location = new System.Drawing.Point(41, 223);
            this.LabelCapacidade.Name = "LabelCapacidade";
            this.LabelCapacidade.Size = new System.Drawing.Size(0, 13);
            this.LabelCapacidade.TabIndex = 8;
            // 
            // ButtonCriarCopo
            // 
            this.ButtonCriarCopo.Location = new System.Drawing.Point(174, 247);
            this.ButtonCriarCopo.Name = "ButtonCriarCopo";
            this.ButtonCriarCopo.Size = new System.Drawing.Size(75, 23);
            this.ButtonCriarCopo.TabIndex = 9;
            this.ButtonCriarCopo.Text = "Criar Copo";
            this.ButtonCriarCopo.UseVisualStyleBackColor = true;
            this.ButtonCriarCopo.Click += new System.EventHandler(this.ButtonCriarCopo_Click);
            // 
            // LabelMensagem
            // 
            this.LabelMensagem.AutoSize = true;
            this.LabelMensagem.Location = new System.Drawing.Point(41, 289);
            this.LabelMensagem.Name = "LabelMensagem";
            this.LabelMensagem.Size = new System.Drawing.Size(0, 13);
            this.LabelMensagem.TabIndex = 10;
            // 
            // TrackBarEncheVaza
            // 
            this.TrackBarEncheVaza.Location = new System.Drawing.Point(158, 370);
            this.TrackBarEncheVaza.Maximum = 100;
            this.TrackBarEncheVaza.Name = "TrackBarEncheVaza";
            this.TrackBarEncheVaza.Size = new System.Drawing.Size(104, 45);
            this.TrackBarEncheVaza.TabIndex = 11;
            this.TrackBarEncheVaza.Scroll += new System.EventHandler(this.TrackBarEncheVaza_Scroll);
            // 
            // LabelContem
            // 
            this.LabelContem.AutoSize = true;
            this.LabelContem.Location = new System.Drawing.Point(41, 370);
            this.LabelContem.Name = "LabelContem";
            this.LabelContem.Size = new System.Drawing.Size(0, 13);
            this.LabelContem.TabIndex = 12;
            // 
            // ButtonVazar
            // 
            this.ButtonVazar.Location = new System.Drawing.Point(174, 320);
            this.ButtonVazar.Name = "ButtonVazar";
            this.ButtonVazar.Size = new System.Drawing.Size(27, 23);
            this.ButtonVazar.TabIndex = 13;
            this.ButtonVazar.Text = "-";
            this.ButtonVazar.UseVisualStyleBackColor = true;
            this.ButtonVazar.Click += new System.EventHandler(this.ButtonVazar_Click);
            // 
            // ButtonEncher
            // 
            this.ButtonEncher.Location = new System.Drawing.Point(235, 320);
            this.ButtonEncher.Name = "ButtonEncher";
            this.ButtonEncher.Size = new System.Drawing.Size(27, 23);
            this.ButtonEncher.TabIndex = 15;
            this.ButtonEncher.Text = "+";
            this.ButtonEncher.UseVisualStyleBackColor = true;
            this.ButtonEncher.Click += new System.EventHandler(this.ButtonEncher_Click);
            // 
            // ButtonVazarTodo
            // 
            this.ButtonVazarTodo.Location = new System.Drawing.Point(108, 320);
            this.ButtonVazarTodo.Name = "ButtonVazarTodo";
            this.ButtonVazarTodo.Size = new System.Drawing.Size(48, 23);
            this.ButtonVazarTodo.TabIndex = 16;
            this.ButtonVazarTodo.Text = "Vazar";
            this.ButtonVazarTodo.UseVisualStyleBackColor = true;
            this.ButtonVazarTodo.Click += new System.EventHandler(this.ButtonVazarTodo_Click);
            // 
            // ButtonEncherTodo
            // 
            this.ButtonEncherTodo.Location = new System.Drawing.Point(278, 320);
            this.ButtonEncherTodo.Name = "ButtonEncherTodo";
            this.ButtonEncherTodo.Size = new System.Drawing.Size(65, 23);
            this.ButtonEncherTodo.TabIndex = 17;
            this.ButtonEncherTodo.Text = "Encher";
            this.ButtonEncherTodo.UseVisualStyleBackColor = true;
            this.ButtonEncherTodo.Click += new System.EventHandler(this.ButtonEncherTodo_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(108, 406);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.progressBar1.Size = new System.Drawing.Size(235, 23);
            this.progressBar1.TabIndex = 18;
            // 
            // LabelValorPercentagen
            // 
            this.LabelValorPercentagen.AutoSize = true;
            this.LabelValorPercentagen.Location = new System.Drawing.Point(171, 441);
            this.LabelValorPercentagen.Name = "LabelValorPercentagen";
            this.LabelValorPercentagen.Size = new System.Drawing.Size(0, 13);
            this.LabelValorPercentagen.TabIndex = 19;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 470);
            this.Controls.Add(this.LabelValorPercentagen);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.ButtonEncherTodo);
            this.Controls.Add(this.ButtonVazarTodo);
            this.Controls.Add(this.ButtonEncher);
            this.Controls.Add(this.ButtonVazar);
            this.Controls.Add(this.LabelContem);
            this.Controls.Add(this.TrackBarEncheVaza);
            this.Controls.Add(this.LabelMensagem);
            this.Controls.Add(this.ButtonCriarCopo);
            this.Controls.Add(this.LabelCapacidade);
            this.Controls.Add(this.LabelEscolhaCapacidade);
            this.Controls.Add(this.ComboBoxQuantidade);
            this.Controls.Add(this.LabelBebida);
            this.Controls.Add(this.LabelEscolhaBebida);
            this.Controls.Add(this.ButtonCerveja);
            this.Controls.Add(this.ButtonAgua);
            this.Controls.Add(this.ButtonIcetea);
            this.Controls.Add(this.ButtonCocacola);
            this.Name = "Form1";
            this.Text = "Bebidas";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarEncheVaza)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonCocacola;
        private System.Windows.Forms.Button ButtonIcetea;
        private System.Windows.Forms.Button ButtonAgua;
        private System.Windows.Forms.Button ButtonCerveja;
        private System.Windows.Forms.Label LabelEscolhaBebida;
        private System.Windows.Forms.Label LabelBebida;
        private System.Windows.Forms.ComboBox ComboBoxQuantidade;
        private System.Windows.Forms.Label LabelEscolhaCapacidade;
        private System.Windows.Forms.Label LabelCapacidade;
        private System.Windows.Forms.Button ButtonCriarCopo;
        private System.Windows.Forms.Label LabelMensagem;
        private System.Windows.Forms.TrackBar TrackBarEncheVaza;
        private System.Windows.Forms.Label LabelContem;
        private System.Windows.Forms.Button ButtonVazar;
        private System.Windows.Forms.Button ButtonEncher;
        private System.Windows.Forms.Button ButtonVazarTodo;
        private System.Windows.Forms.Button ButtonEncherTodo;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label LabelValorPercentagen;
    }
}

