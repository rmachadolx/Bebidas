﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Bebidas
{
    public partial class Form1 : Form
    {

        Copo copoServido = new Copo();
        public Form1()
        {
            InitializeComponent();
            ButtonEncher.Enabled = false;
            ButtonEncherTodo.Enabled = false;
            ButtonVazar.Enabled = false;
            ButtonVazarTodo.Enabled = false;
            ComboBoxQuantidade.Enabled = false;
            ButtonCriarCopo.Enabled = false;
            LabelEscolhaCapacidade.Visible = false;
            TrackBarEncheVaza.Enabled = false;
            ComboBoxQuantidade.SelectedIndex = 0;
            LabelCapacidade.Visible = false;


        }

        private void ButtonCocacola_Click(object sender, EventArgs e)
        {

            LabelBebida.Text = "A bebida escolihida foi Coca-Cola";
            copoServido.Liquido = "Coca-Ccola";
            ButtonCocacola.Enabled = false;
            ButtonIcetea.Enabled = true;
            ButtonAgua.Enabled = true;
            ButtonCerveja.Enabled = true;
            LabelEscolhaCapacidade.Visible = true;
            ComboBoxQuantidade.Enabled = true;
        }


        private void ButtonIcetea_Click(object sender, EventArgs e)
        {
            LabelBebida.Text = "A bebida escolihida foi Ice Tea";
            copoServido.Liquido = "Ice Tea";
            ButtonCocacola.Enabled = true;
            ButtonIcetea.Enabled = false;
            ButtonAgua.Enabled = true;
            ButtonCerveja.Enabled = true;
            LabelEscolhaCapacidade.Visible = true;
            ComboBoxQuantidade.Enabled = true;
        }

        private void ButtonAgua_Click(object sender, EventArgs e)
        {
            LabelBebida.Text = "A bebida escolihida foi Água";
            copoServido.Liquido = "Água";
            ButtonCocacola.Enabled = true;
            ButtonIcetea.Enabled = true;
            ButtonAgua.Enabled = false;
            ButtonCerveja.Enabled = true;
            LabelEscolhaCapacidade.Visible = true;
            ComboBoxQuantidade.Enabled = true;
        }

        private void ButtonCerveja_Click(object sender, EventArgs e)
        {
            LabelBebida.Text = "A bebida escolihida foi Cerveja";
            copoServido.Liquido = "Cerveja";
            ButtonCocacola.Enabled = true;
            ButtonIcetea.Enabled = true;
            ButtonAgua.Enabled = true;
            ButtonCerveja.Enabled = false;
            LabelEscolhaCapacidade.Visible = true;
            ComboBoxQuantidade.Enabled = true;

        }

        private void ComboBoxQuantidade_SelectedIndexChanged(object sender, EventArgs e)
        {
            LabelCapacidade.ForeColor = Color.Black;
            LabelCapacidade.Visible = true;
            int indice = 0;

            string quantidade = "";
            indice = ComboBoxQuantidade.SelectedIndex;
            switch (indice)
            {
                case 0:
                    LabelCapacidade.ForeColor = Color.Red;
                    LabelCapacidade.Text = "Escolha a capacidade!";
                    return;
                   
                case 1:
                    quantidade = "20 cl";
                    copoServido.Capacidade = 20.0;

                    break;
                case 2:
                    quantidade = "33 cl";
                    copoServido.Capacidade = 33.0;
                    break;
                case 3:
                    quantidade = "50 cl";
                    copoServido.Capacidade = 50.0;
                    break;
                case 4:
                    quantidade = "75 cl";
                    copoServido.Capacidade = 75.0;
                    break;
               
            }
            LabelCapacidade.Text = "A capacidade escolhida foi " + quantidade;
            ButtonCriarCopo.Enabled = true;
        }

        private void ButtonCriarCopo_Click(object sender, EventArgs e)
        {
            copoServido = new Copo(copoServido.Liquido,copoServido.Capacidade);
            copoServido.Encher(copoServido.Capacidade);
            double teste = copoServido.Contem;
            TrackBarEncheVaza.Value = copoServido.ValorEmPercentagem();
            progressBar1.Value = copoServido.ValorEmPercentagem();
            LabelValorPercentagen.Text = "Valor em percentagem: " + copoServido.ValorEmPercentagem().ToString() + "%";
            LabelMensagem.Text = copoServido.ToString();
            ButtonEncher.Enabled = true;
            ButtonEncherTodo.Enabled = true;
            ButtonVazar.Enabled = true;
            ButtonVazarTodo.Enabled = true;
            TrackBarEncheVaza.Enabled = true;
        }

        private void TrackBarEncheVaza_Scroll(object sender, EventArgs e)
        {

            double conteudo = 0.0d;
            conteudo = copoServido.Capacidade * TrackBarEncheVaza.Value / 100;
            copoServido.Contem = conteudo;

            progressBar1.Value = copoServido.ValorEmPercentagem();
            LabelValorPercentagen.Text = "Valor em percentagem: " + copoServido.ValorEmPercentagem().ToString() + "%";
            LabelMensagem.Text = copoServido.ToString();
        }

        private void ButtonVazar_Click(object sender, EventArgs e)
        {


            copoServido.Esvaziar(1.0);
            TrackBarEncheVaza.Value = copoServido.ValorEmPercentagem();
            progressBar1.Value = copoServido.ValorEmPercentagem();
            LabelValorPercentagen.Text = "Valor em percentagem: " + copoServido.ValorEmPercentagem().ToString() + "%";
            LabelMensagem.Text = copoServido.ToString();
        }

        private void ButtonEncher_Click(object sender, EventArgs e)
        {
            copoServido.Encher(1.0);
            TrackBarEncheVaza.Value = copoServido.ValorEmPercentagem();
            progressBar1.Value = copoServido.ValorEmPercentagem();
            LabelValorPercentagen.Text = "Valor em percentagem: " + copoServido.ValorEmPercentagem().ToString() + "%";
            LabelMensagem.Text = copoServido.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ButtonVazarTodo_Click(object sender, EventArgs e)
        {
            copoServido.Esvaziar(copoServido.Capacidade);
            TrackBarEncheVaza.Value = copoServido.ValorEmPercentagem();
            progressBar1.Value = copoServido.ValorEmPercentagem();
            LabelValorPercentagen.Text = "Valor em percentagem: " + copoServido.ValorEmPercentagem().ToString() + "%";
            LabelMensagem.Text = copoServido.ToString();
        }

        private void ButtonEncherTodo_Click(object sender, EventArgs e)
        {
            copoServido.Encher(copoServido.Capacidade);
            TrackBarEncheVaza.Value = copoServido.ValorEmPercentagem();
            progressBar1.Value = copoServido.ValorEmPercentagem();
            LabelValorPercentagen.Text = "Valor em percentagem: " + copoServido.ValorEmPercentagem().ToString() + "%";
            LabelMensagem.Text = copoServido.ToString();
        }


    }
}
